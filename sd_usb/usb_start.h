/*
 * Code not generated from Atmel Start.
 *
 * This file could be overwritten when reconfiguring your Atmel Start project.
 * Please keep unchecked the overwrite box when loading new configuration
 */
#ifndef USB_DEVICE_MAIN_H
#define USB_DEVICE_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "mscdf.h"
#include "mscdf_desc.h"
#include "sd_mmc.h"

/**
 * @brief Atmel Start initialization function
 * 
 */
void usbd_msc_init(void);

/**
 * @brief Links callback functions to USB events
 * 
 * @param usbdisk_buf Buffer for usb disk
 */
void mscdf_callback_init(uint8_t *usbdisk_buf);
/**
 * \brief Non-blocking task that is invoked in main loop, to process USB and disk tasks
 */
void mscdf_task(void);

/**
 * \brief Initialize USB
 */
void usb_init(void);

/**
 * @brief USB connection states
 * 
 */
enum usb_hls_t 
{
	/* Disconnected */
	USB_IDLE = 0,
	/* Connected */
	USB_MODE,
	/* Initializing */
	USB_INIT,
	/* Deinitializing */
	USB_DEINIT
};

/**
 * @brief 	Handler for WatchDog Timer Early Interrupt
 * 
 */
void WDT_Handler(void );

/**
 * @brief 	Configures WatchDog Timer and enables Early Interrupt
 * 
 */
void wdt_config();

/**
 * @brief 	Non-blocking task to process USB tasks only
 * 
 */
void usb_loop();

/**
 * @brief 	Request to turn off the USB connection
 * 
 * @return true 	Successfully
 * @return false 	USB connection isn't running
 */
bool usb_stop();

/**
 * @brief 	Request to turn on the USB connection
 * 
 * @return true 	Successfully
 * @return false 	USB connection is already running
 */
bool usb_go();

/**
 * @brief Get the usb state object
 * 
 * @return enum usb_hls_t 
 */
enum usb_hls_t get_usb_state();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // USB_DEVICE_MAIN_H
