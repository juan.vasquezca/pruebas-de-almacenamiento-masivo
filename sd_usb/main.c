
#include <atmel_start.h>


void extIRQ_reg(void);

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	/* Undo usb init in atmel_start_init */
	mscdf_deinit();
	usbdc_deinit();
	/*Power pins off */
	gpio_set_pin_level(PWR_SD_LDO, false);
	gpio_set_pin_level(PWR_HPWR, false);
	
	gpio_set_pin_level(LED_R, true);
	gpio_set_pin_level(LED_G, true);
	gpio_set_pin_level(LED_B, false);
	/*Disable Watchdog Timer*/
	wdt_disable(&WDT_0);
	/*Button interrupt register*/
	extIRQ_reg();

	delay_ms(500);
	/* Set reset time and interrupt time of Watchdog Timer*/
	wdt_config();
	
	while (1) 
	{	
		switch (get_usb_state())
		{
			case (USB_IDLE):
			{
				gpio_set_pin_level(LED_R, false);
				gpio_set_pin_level(LED_G, true);
				gpio_set_pin_level(LED_B, true);
				break;
			}
			case (USB_INIT):
			{
				gpio_set_pin_level(LED_R, false);
				gpio_set_pin_level(LED_G, false);
				gpio_set_pin_level(LED_B, true);
				break;
			}
			case (USB_MODE):
			{
				gpio_set_pin_level(LED_R, true);
				gpio_set_pin_level(LED_G, false);
				gpio_set_pin_level(LED_B, true);
				break;
			}
			case (USB_DEINIT):
			{
				gpio_set_pin_level(LED_R, true);
				gpio_set_pin_level(LED_G, false);
				gpio_set_pin_level(LED_B, false);
				break;
			}
			
		}
		
		/* USBMSD task */
		usb_loop();
	}
}


static void btnUSBToggle(void)
{
	if (get_usb_state() == USB_IDLE)
	{
		/* Start USB mode*/
		usb_go();
	}
	if (get_usb_state() == USB_MODE)
	{
		/* Stop USB mode*/
		usb_stop();
	}
}

static void btnUSBStop(void)
{
	
}


void extIRQ_reg(void)
{
	ext_irq_register(PIN_PA05, btnUSBToggle);
	ext_irq_register(PIN_PA06, btnUSBStop);
}