#include <atmel_start.h>

static uint32_t msc_ramdisk_buf[CONF_USB_MSC_LUN0_CAPACITY * 1024 / 4];
static uint32_t msc_diskcache_buf[CONF_USB_MSC_LUN_BUF_SECTORS * 512 / 4];


#if CONF_USBD_HS_SP
static uint8_t single_desc_bytes[] = {
	/* Device descriptors and Configuration descriptors list. */
MSC_HS_DESCES_LS_FS};
static uint8_t single_desc_bytes_hs[] = {
	/* Device descriptors and Configuration descriptors list. */
MSC_HS_DESCES_HS};
#else
static uint8_t single_desc_bytes[] = {
	/* Device descriptors and Configuration descriptors list. */
MSC_DESCES_LS_FS};
#endif


static struct usbd_descriptors single_desc[]
= {{single_desc_bytes, single_desc_bytes + sizeof(single_desc_bytes)}
#if CONF_USBD_HS_SP
,
{single_desc_bytes_hs, single_desc_bytes_hs + sizeof(single_desc_bytes_hs)}
#endif
};

enum sm_t 
{
	IDLE = 0,
	USB_CONNECTED,
	USB_DISCONNECTED
};

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	gpio_set_pin_level(PWR_HPWR, true);
	gpio_set_pin_level(PWR_SD_LDO,true);
	///* Initializes USB interrupt tasks */
	mscdf_demo_init((uint8_t *)msc_ramdisk_buf, (uint8_t *)msc_diskcache_buf);
	///* Start USB*/
	usbdc_start(single_desc);
	///* USB attach */
	usbdc_attach();
//
	while (!mscdf_is_enabled()) {
		///* wait massive storage to be installed */
	};
//
	while (1) {
		mscdf_demo_task();
		//
	}
}