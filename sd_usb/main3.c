#include <atmel_start.h>

static uint32_t msc_ramdisk_buf[CONF_USB_MSC_LUN0_CAPACITY * 1024 / 4];
static uint32_t msc_diskcache_buf[CONF_USB_MSC_LUN_BUF_SECTORS * 512 / 4];


#if CONF_USBD_HS_SP
static uint8_t single_desc_bytes[] = {
	/* Device descriptors and Configuration descriptors list. */
MSC_HS_DESCES_LS_FS};
static uint8_t single_desc_bytes_hs[] = {
	/* Device descriptors and Configuration descriptors list. */
MSC_HS_DESCES_HS};
#else
static uint8_t single_desc_bytes[] = {
	/* Device descriptors and Configuration descriptors list. */
MSC_DESCES_LS_FS};
#endif

//#define ATTACH_METHOD
//#define CHECK_ADDEN_BIT
//#define MSCDF_FUNCTION
#define CHECK_USB_FSM

static struct usbd_descriptors single_desc[]
= {{single_desc_bytes, single_desc_bytes + sizeof(single_desc_bytes)}
#if CONF_USBD_HS_SP
,
{single_desc_bytes_hs, single_desc_bytes_hs + sizeof(single_desc_bytes_hs)}
#endif
};

enum sm_t 
{
	IDLE = 0,
	USB_CONNECTED,
	USB_DISCONNECTED
};
enum sm_t stDetector(void );

static int32_t testFunction( enum usbdc_change_type change, uint32_t value);

static struct usbdc_handler ch_handler = {NULL, (FUNC_PTR)testFunction}; 

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	gpio_set_pin_level(LED_R, true);
	gpio_set_pin_level(LED_G, true);
	gpio_set_pin_level(LED_B, true);
	gpio_set_pin_level(PWR_SD_LDO, true);
	gpio_set_pin_level(PWR_HPWR, true);
	
	///* Initializes USB interrupt tasks */
	mscdf_demo_init((uint8_t *)msc_ramdisk_buf, (uint8_t *)msc_diskcache_buf);
	//usbdc_register_handler(USBDC_HDL_CHANGE, &ch_handler);
	
	///* Start USB*/
	usbdc_start(single_desc);
	gpio_set_pin_level(LED_B, false);
	delay_ms(500);
	gpio_set_pin_level(LED_B, true);
	///* USB attach */
	usbdc_attach();
	
	while (!mscdf_is_enabled()) {
		///* wait massive storage to be installed */
	};
	static bool sd_on = true;
	while (1) {
		switch (stDetector())
		{
			case (IDLE):
			{
				gpio_set_pin_level(LED_R,true);
				gpio_set_pin_level(LED_G,true);
				gpio_set_pin_level(LED_B,false);
				break;
			}
			case (USB_CONNECTED):
			{
				gpio_set_pin_level(LED_R,true);
				gpio_set_pin_level(LED_G,false);
				gpio_set_pin_level(LED_B,true);
				sd_on = true;
				gpio_set_pin_level(PWR_HPWR, true);
				//gpio_set_pin_level(PWR_SD_LDO, true);
				//while (!mscdf_is_enabled()){};
				mscdf_demo_task();
				break;
			}
			case (USB_DISCONNECTED):
			{
				gpio_set_pin_level(LED_R,false);
				gpio_set_pin_level(LED_G,true);
				gpio_set_pin_level(LED_B,true);
				if (sd_on && sd_xfer_done())
				{
					sd_mmc_wait_end_of_read_blocks(false);
					sd_mmc_wait_end_of_write_blocks(false);
					gpio_set_pin_level(PWR_HPWR, false);
					//gpio_set_pin_level(PWR_SD_LDO, false);
					sd_on = false;
				}
				else if (sd_on)
				{
					mscdf_demo_task();
				}
				/*if (sd_xfer_done())
				{
					
				}
				else
				{
					//mscdf_demo_task();
				}*/
				break;
			}
			default:
			{
				gpio_set_pin_level(LED_R,true);
				gpio_set_pin_level(LED_G,true);
				gpio_set_pin_level(LED_B,true);
			}
		}
		//mscdf_demo_task();
		//delay_ms(50);
		
	}
}

enum sm_t stDetector(void )
{
	enum sm_t ledStatus;
	bool condition = false;

	#ifdef ATTACH_METHOD
	condition = !hri_usbdevice_get_CTRLB_DETACH_bit(USB);
	#endif

	#ifdef CHECK_ADDEN_BIT
	condition = hri_usbdevice_get_DADD_ADDEN_bit(USB);
	#endif

	#ifdef MSCDF_FUNCTION
	condition = mscdf_is_enabled();
	#endif

	#ifdef CHECK_USB_FSM
	condition = (hri_usb_read_FSMSTATUS_reg(USB) & 0x32);
	#endif

	if (condition)
	{
		ledStatus = USB_CONNECTED;
	}
	else
	{
		ledStatus = USB_DISCONNECTED;
	}
	return ledStatus;
}

static int32_t testFunction( enum usbdc_change_type change, uint32_t value)
{
	static int32_t counter, connected;
	if (change == USBDC_C_CONN)
	{
		connected++;
	}
	counter++;
	gpio_toggle_pin_level(LED_B);
	
	return 0;
}