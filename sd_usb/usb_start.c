
/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file or main.c
 * to avoid loosing it when reconfiguring.
 */
#include "usb_start.h"
#include "atmel_start.h"


/* Max LUN number */
#define CONF_USB_MSC_MAX_LUN 0

#if CONF_USB_MSC_LUN_DEMO

/* MSC LUN number */
#define LUN_SD 0

/* MSC LUN definitions */
#define DISK_INFORMATION(n)                                                                                            \
	{                                                                                                                  \
		CONF_USB_MSC_LUN##n##_TYPE, (CONF_USB_MSC_LUN##n##_RMB << 7),                                                  \
		    ((CONF_USB_MSC_LUN##n##_ISO << 6) + (CONF_USB_MSC_LUN##n##_ECMA << 3) + CONF_USB_MSC_LUN##n##_ANSI),       \
		    CONF_USB_MSC_LUN##n##_REPO, 31, 0x00, 0x00, 0x00, CONF_USB_MSC_LUN##n##_FACTORY,                           \
		    CONF_USB_MSC_LUN##n##_PRODUCT, CONF_USB_MSC_LUN##n##_PRODUCT_VERSION                                       \
	}

#define DISK_CAPACITY(n)                                                                                               \
	{                                                                                                                  \
		(uint8_t)(CONF_USB_MSC_LUN##n##_LAST_BLOCK_ADDR >> 24),                                                        \
		    (uint8_t)(CONF_USB_MSC_LUN##n##_LAST_BLOCK_ADDR >> 16),                                                    \
		    (uint8_t)(CONF_USB_MSC_LUN##n##_LAST_BLOCK_ADDR >> 8),                                                     \
		    (uint8_t)(CONF_USB_MSC_LUN##n##_LAST_BLOCK_ADDR >> 0),                                                     \
		    (uint8_t)((uint32_t)(CONF_USB_MSC_LUN##n##_BLOCK_SIZE) >> 24),                                             \
		    (uint8_t)((uint32_t)(CONF_USB_MSC_LUN##n##_BLOCK_SIZE) >> 16),                                             \
		    (uint8_t)((uint32_t)(CONF_USB_MSC_LUN##n##_BLOCK_SIZE) >> 8),                                              \
		    (uint8_t)((uint32_t)(CONF_USB_MSC_LUN##n##_BLOCK_SIZE) >> 0)                                               \
	}

/* MSC LUN settings */

static uint8_t *disk_sect_buf;

static uint8_t       msc_lun;
static bool          xfer_dir;
static volatile bool usb_busy;
static uint32_t      disk_addr;
static uint32_t      usb_remain = 0;

static volatile bool disk_busy;
static bool          disk_rw_init = false;
static uint8_t       n_buf_rdy    = 0;
static uint8_t       disk_sect_i, usb_sect_i;
static uint32_t      usb_addr;
static uint32_t      disk_remain = 0;
volatile enum usb_hls_t	usb_state = USB_IDLE;
volatile uint8_t 	 wdt_count = 0;
static uint32_t msc_diskcache_buf[CONF_USB_MSC_LUN_BUF_SECTORS * 512 / 4];




/* Inquiry Information */
static uint8_t inquiry_info[CONF_USB_MSC_MAX_LUN + 1][36] = {DISK_INFORMATION(1)};

/* Capacities of Disk */
static uint8_t format_capa[CONF_USB_MSC_MAX_LUN + 1][8] = {DISK_CAPACITY(1)};

#endif

#if CONF_USBD_HS_SP
static uint8_t single_desc_bytes[] = {
    /* Device descriptors and Configuration descriptors list. */
    MSC_HS_DESCES_LS_FS};
static uint8_t single_desc_bytes_hs[] = {
    /* Device descriptors and Configuration descriptors list. */
    MSC_HS_DESCES_HS};
#else
static uint8_t single_desc_bytes[] = {
    /* Device descriptors and Configuration descriptors list. */
    MSC_DESCES_LS_FS};
#endif

static struct usbd_descriptors single_desc[]
    = {{single_desc_bytes, single_desc_bytes + sizeof(single_desc_bytes)}
#if CONF_USBD_HS_SP
       ,
       {single_desc_bytes_hs, single_desc_bytes_hs + sizeof(single_desc_bytes_hs)}
#endif
};

/* Ctrl endpoint buffer */
static uint8_t ctrl_buffer[64];

#if CONF_USB_MSC_LUN_DEMO

/**
 * \brief Eject Disk
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static int32_t msc_disk_eject(uint8_t lun)
{
	if (lun > CONF_USB_MSC_MAX_LUN) {
		return ERR_NOT_FOUND;
	}
	return ERR_NONE;
}

/**
 * \brief Inquiry whether Disk is ready
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static int32_t msc_disk_is_ready(uint8_t lun)
{
	if (lun > CONF_USB_MSC_MAX_LUN) {
		return ERR_NOT_FOUND;
	}
	return ERR_NONE;
}

/**
 * \brief Callback invoked when a new read blocks command received
 * \param[in] lun logic unit number
 * \param[in] addr start address of disk to be read
 * \param[in] nblocks block amount to be read
 * \return Operation status.
 */
static int32_t msc_new_read(uint8_t lun, uint32_t addr, uint32_t nblocks)
{
	int32_t  rc  = msc_disk_is_ready(lun);
	uint32_t lba = 0;
	if (rc != ERR_NONE) {
		return rc;
	}
	lba = (format_capa[lun][0] << 24) + (format_capa[lun][1] << 16) + (format_capa[lun][2] << 8) + format_capa[lun][3];
	if (addr > lba || addr + nblocks > lba + 1) {
		return ERR_BAD_ADDRESS;
	}
	msc_lun  = lun;
	xfer_dir = true;

	disk_addr  = addr;
	usb_remain = nblocks;

	disk_rw_init = true;
	usb_addr     = addr;
	disk_remain  = nblocks;
	disk_sect_i  = 0;
	usb_sect_i   = 0;

	return ERR_NONE;
}

/**
 * \brief Callback invoked when a new write blocks command received
 * \param[in] lun logic unit number
 * \param[in] addr start address of disk to be written
 * \param[in] nblocks block amount to be written
 * \return Operation status.
 */
static int32_t msc_new_write(uint8_t lun, uint32_t addr, uint32_t nblocks)
{
	int32_t  rc  = msc_disk_is_ready(lun);
	uint32_t lba = 0;
	if (rc != ERR_NONE) {
		return rc;
	}
	lba = (format_capa[lun][0] << 24) + (format_capa[lun][1] << 16) + (format_capa[lun][2] << 8) + format_capa[lun][3];
	if (addr > lba || addr + nblocks > lba + 1) {
		return ERR_BAD_ADDRESS;
	}
	msc_lun  = lun;
	xfer_dir = false;

	disk_addr  = addr;
	usb_remain = nblocks;

	disk_rw_init = true;
	usb_addr     = addr;
	disk_remain  = nblocks;
	disk_sect_i  = 0;
	usb_sect_i   = 0;

	return ERR_NONE;
}

/**
 * \brief Callback invoked when a blocks transfer is done
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static int32_t msc_xfer_done(uint8_t lun)
{
	if (lun > CONF_USB_MSC_MAX_LUN) {
		return ERR_DENIED;
	}
	usb_busy = false;
	if (0 != --usb_remain) {
		usb_addr++;
		if (CONF_USB_MSC_LUN_BUF_SECTORS == ++usb_sect_i) {
			usb_sect_i = 0;
		}
	}
	if (xfer_dir) {
		n_buf_rdy--;
	} else {
		n_buf_rdy++;
	}
	return ERR_NONE;
}
/**
 * \brief SD/MMC Disk loop
 */
static inline void msc_sdmmc_task(void)
{
	hal_atomic_t flag;
	if (msc_lun != LUN_SD) {
		return;
	}
	if (!mscdf_is_enabled() && disk_busy) {
		disk_busy = false;
		sd_mmc_wait_end_of_read_blocks(true);
		sd_mmc_wait_end_of_write_blocks(true);
		disk_remain = 0;
		return;
	}
	if (disk_busy) {
		/* Wait disk accessing done */
		if (xfer_dir) {
			if (SD_MMC_OK == sd_mmc_wait_end_of_read_blocks(false)) {
				disk_busy = false;
				disk_remain--;
				disk_addr++;
				if (++disk_sect_i == CONF_USB_MSC_LUN_BUF_SECTORS) {
					disk_sect_i = 0;
				}
				atomic_enter_critical(&flag);
				n_buf_rdy++;
				atomic_leave_critical(&flag);
			}
		} else {
			if (SD_MMC_OK == sd_mmc_wait_end_of_write_blocks(false)) {
				disk_busy = false;
				disk_remain--;
				disk_addr++;
				if (++disk_sect_i == CONF_USB_MSC_LUN_BUF_SECTORS) {
					disk_sect_i = 0;
				}
				atomic_enter_critical(&flag);
				n_buf_rdy--;
				atomic_leave_critical(&flag);

				if (disk_remain == 0) {
					/* Terminate write */
					mscdf_xfer_blocks(false, NULL, 0);
				}
			}
		}
		return;
	}
	if (disk_remain) {
		if (xfer_dir) {
			/* Read Disk Process */
			if (n_buf_rdy < CONF_USB_MSC_LUN_BUF_SECTORS) {
				disk_busy = true;
				if (disk_rw_init) {
					disk_rw_init = false;
					atomic_enter_critical(&flag);
					sd_mmc_init_read_blocks(0, disk_addr, disk_remain);
					sd_mmc_start_read_blocks(&disk_sect_buf[disk_sect_i << 9], 1);
					atomic_leave_critical(&flag);
				} else {
					sd_mmc_start_read_blocks(&disk_sect_buf[disk_sect_i << 9], 1);
				}
			}
		} else {
			/* Write Disk Process */
			if (n_buf_rdy > 0) {
				disk_busy = true;
				if (disk_rw_init) {
					disk_rw_init = false;
					sd_mmc_init_write_blocks(0, disk_addr, disk_remain);
				}
				sd_mmc_start_write_blocks(&disk_sect_buf[disk_sect_i << 9], 1);
			}
		}
	}
}
/**
 * \brief Disk loop
 */
static void msc_disk_task(void)
{
	msc_sdmmc_task();
}

/**
 * \brief USB loop
 */
static void msc_usb_task(void)
{
	if (false == usb_busy && 0 != usb_remain) {
		if (0 == xfer_dir) {
			/* Write Disk Request */
			if (n_buf_rdy < CONF_USB_MSC_LUN_BUF_SECTORS) {
				usb_busy = true;
				mscdf_xfer_blocks(false, &disk_sect_buf[usb_sect_i << 9], 1);
			}
		} else {
			/* Read Disk Request */
			if (n_buf_rdy > 0) {
				usb_busy = true;
				mscdf_xfer_blocks(true, &disk_sect_buf[usb_sect_i << 9], 1);
			}
		}
	}
}

/**
 * \brief Callback invoked when inquiry data command received
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static uint8_t *msc_inquiry_info(uint8_t lun)
{
	if (lun > CONF_USB_MSC_MAX_LUN) {
		return NULL;
	} else {
		disk_busy   = false;
		disk_remain = 0;
		n_buf_rdy   = 0x00;
		usb_busy    = false;
		usb_remain  = 0;
		return &inquiry_info[lun][0];
	}
}

/**
 * \brief Callback invoked when read format capacities command received
 * \param[in] lun logic unit number
 * \return Operation status.
 */
static uint8_t *msc_get_capacity(uint8_t lun)
{
	if (lun > CONF_USB_MSC_MAX_LUN) {
		return NULL;
	} else {
		uint32_t cap        = sd_mmc_get_capacity(0) * 2 - 1;
		format_capa[lun][0] = (uint8_t)(cap >> 24);
		format_capa[lun][1] = (uint8_t)(cap >> 16);
		format_capa[lun][2] = (uint8_t)(cap >> 8);
		format_capa[lun][3] = (uint8_t)(cap >> 0);
		return &format_capa[lun][0];
	}
}
/**
 * @brief Add callbacks for usb events
 * 
 * @param usbdisk_buf 
 * @return * void 
 */

void mscdf_callback_init( uint8_t *usbdisk_buf)
{
	disk_sect_buf = usbdisk_buf;
	mscdf_register_callback(MSCDF_CB_INQUIRY_DISK, (FUNC_PTR)msc_inquiry_info);
	mscdf_register_callback(MSCDF_CB_GET_DISK_CAPACITY, (FUNC_PTR)msc_get_capacity);
	mscdf_register_callback(MSCDF_CB_START_READ_DISK, (FUNC_PTR)msc_new_read);
	mscdf_register_callback(MSCDF_CB_START_WRITE_DISK, (FUNC_PTR)msc_new_write);
	mscdf_register_callback(MSCDF_CB_EJECT_DISK, (FUNC_PTR)msc_disk_eject);
	mscdf_register_callback(MSCDF_CB_TEST_DISK_READY, (FUNC_PTR)msc_disk_is_ready);
	mscdf_register_callback(MSCDF_CB_XFER_BLOCKS_DONE, (FUNC_PTR)msc_xfer_done);
}

void mscdf_task(void)
{
	msc_disk_task();
	msc_usb_task();
}

#endif

/**
 * \brief USB MSC Init
 */
void usbd_msc_init(void)
{
	/* usb stack init */
	usbdc_init(ctrl_buffer);

	/* usbdc_register_funcion inside */
	mscdf_init(CONF_USB_MSC_MAX_LUN);
}


void usb_init(void)
{

	usbd_msc_init();
}

/**
 * @brief 	Initializes USB and SD device for MSD function
 * 		  	Requires WDT configured prior
 * 			PWR_HPWR adn PWR_SD_LDO must been enabled as outputs
 * 
 */

void usb_sd_init()
{
	/* Enables WatchDog Timer */
	wdt_enable(&WDT_0);
	/* USB Init */
	usb_init();
	sd_mmc_stack_init();
	gpio_set_pin_level(PWR_HPWR, true);
	gpio_set_pin_level(PWR_SD_LDO, true);
	mscdf_callback_init((uint8_t *)msc_diskcache_buf);
	while (sd_mmc_check(0) != SD_MMC_OK) {
		/* Requires SD/MMC inserted before start */
	}
	///* Start USB*/
	usbdc_start(single_desc);
	
	///* USB attach */
	usbdc_attach();
	
	while (!mscdf_is_enabled()) {
		///* wait massive storage to be installed */
	};
}

/**
 * @brief Deinit USBMSD function
 * 
 */
void usb_sd_deinit()
{
	/* Stop USB */
	usbdc_stop();
	/* USB detach */
	usbdc_detach();
	usbUnconfig();
	/* PWR OFF SD */
	usbdc_deinit();
	mscdf_deinit();
	gpio_set_pin_level(PWR_HPWR, false);
	gpio_set_pin_level(PWR_SD_LDO, false);
	/* Disables WatchDog Timer */
	wdt_disable(&WDT_0);
}

/**
 * @brief 	Request to turn on USB connection
 * 
 * @retval true		Success, initialization will start in next usb_loop() call
 * @retval false 	USB connection is already running
 */
bool usb_go()
{
	if (usb_state == USB_IDLE)
	{
		usb_state = USB_INIT;
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @brief Request to turn off USB connection
 * 
 * @retval true		Success, USB connection will be deinitialized in next usb_loop() call
 * @retval false 	USB connection isn't running
 */
bool usb_stop()
{
	if (usb_state == USB_MODE)
	{
		usb_state = USB_DEINIT;
		return true;
	}
	else
	{
		return false;
	}
}

/**
 * @brief 	Manages USB connection.
 * 			Must be called repeatedly in a while loop
 * 
 */
void usb_loop()
{
	switch(usb_state)
	{
		case (USB_IDLE):
		{
			/* Reset WatchDog interrupt count */
			wdt_count = 0;
			break;
		}
		case (USB_INIT):
		{
			/* Start initialization routine */
			usb_sd_init();
			/* Go to next state */
			usb_state = USB_MODE;
			break;
		}
		case (USB_MODE):
		{
			/* Reset WatchDog interrupt count */
			wdt_count = 0;
			/* Massive Storage Device Task */
			mscdf_task();
			break;
		}
		case (USB_DEINIT):
		{
			/* Cancel reading and writing operations */
			sd_mmc_wait_end_of_read_blocks(true);
			sd_mmc_wait_end_of_write_blocks(true);
			/* Start deinitialization routine*/
			usb_sd_deinit();
			/* Back to inactive state */
			usb_state = USB_IDLE;
			break;
		}
	}
}

/**
 * @brief 	Set WatchDog timer recommended configuration. 
 * 			Enables WatchDog Early Interrupt
 * 
 */
void wdt_config()
{
	/* Set normal operation mode */
	hri_wdt_write_CTRLA_WEN_bit(WDT,false);
	/* Set time out period to 8192 clock cycles (fclk = 1024 Hz) */
	hri_wdt_write_CONFIG_PER_bf(WDT,0x9);
	/* Set early warning interrupt time offset to 4096 cycles */
	hri_wdt_write_EWCTRL_EWOFFSET_bf(WDT,0x8);
	/* Enable Early Warning Interrupt */
	hri_wdt_write_INTEN_EW_bit(WDT,true);
	/* Enable CPU interrupt */
	NVIC_DisableIRQ(WDT_IRQn);
	NVIC_ClearPendingIRQ(WDT_IRQn);
	NVIC_EnableIRQ(WDT_IRQn);
}

/**
 * @brief 	WatchDog Timer Early Interrupt Task.
 * 			Check if the device is spending much time in init or deinit.
 * 
 */
void WDT_Handler(void )
{
	if (usb_state == USB_INIT || usb_state == USB_DEINIT)
	{
		/* Reset WatchDog Timer only once */
		if (wdt_count < 1)
		{
			wdt_feed(&WDT_0);
		}
		/* Count calls to interrupt in init or deinit mode */
		wdt_count++;
	}
	else
	{
		/* Device running, reset WatchDog Timer*/
		wdt_feed(&WDT_0);
	}
	/* Clear interrupt flag */
	hri_wdt_clear_INTFLAG_EW_bit(WDT);
}

/**
 * @brief Get the usb state object
 * 
 * @return enum usb_hls_t 
 */
enum usb_hls_t get_usb_state()
{
	return usb_state;
}