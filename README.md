# Pruebas de almacenamiento masivo

Proyecto para alojar pruebas relativas al funcionamiento del ATSAMD51J20A como dispositivo de almacenamiento masivo usando una tarjeta micro sd.

## Señalización
- Se usa la siguiente señalización de estados en el LED RGB:
  - ROJO: Modo USB apagado. Estado por defecto
  - AMARILLO: Inicializando modo USB.
  - VERDE: Modo USB activo. No implica necesariamente una conexión USB.
  - CIAN: Deinicializando modo USB.
  - AZUL: Inicio microcontrolador.
 
## Comportamiento normal

- Al iniciar, se enciende el color azul durante 500 ms.
- Al pulsar el botón 1, se puede alternar entre la entrada y salida del modo USB.
- No es posible entrar en modo USB si no hay un host conectado o falta la tarjeta SD.
- Si el dispositivo se expulsa desde el host, es posible volver a establecer una conexión reconectando el cable sin necesidad de salir del modo USB.
- Es posible salir y volver a entrar del modo USB con el dispositivo conectado físicamente, usando el botón 1 y respetando el tiempo de inicialización del bus.
- No es posible establecer una conexión sin activar el modo USB antes. No insista.


## Casos de error 
- Si se sale del modo USB antes de completar la configuración del bus, no es posible volver a entrar al modo USB de nuevo.
- Si el programa pasa más de 4 segundos estancado inicializando o deinicializando, se detecta el error y se reinicia el microcontrolador.
- Un reinicio por detección de error no debe tomar más de 10 segundos.
- Si el dispositivo se desconecta en medio de una transferencia de datos, se reinicia el funcionamiento.
- Si por causa de un error, una interrupción se prolonga demasiado, se detecta el error y el dispositivo es reiniciado.
- Por simplicidad, se considera el intento por entrar al modo USB sin conectar físicamente el dispositivo como un error, por lo que deriva en un reinicio.

## Requisitos para integrar:
- Configurar Atmel Start usando de base la configuración incluida en el repositorio.
- Cuidar el nombre de los pines de poder `PWR_SD_LDO` y `PWR_HPWR`.
- El botón a usar puede ser cambiado. La asignación de la función de interrupción no están incluida en el driver, por lo que se debe hacer en `main.c` u otro archivo.
- Reemplazar los archivos `usb_start.c` y `usb_start.h` generados automáticamente.
- En `main` es necesario:
  - Desinicializar lo hecho por `atmel_start_init()` llamando a `mscdf_deinit()` y	`usbdc_deinit()`.
  - Setear los valores iniciales de los pines `PWR_SD_LDO` y `PWR_HPWR`.
  - Deshabilitar el WatchDog Timer.
  - Registrar la función de interrupción asociada al botón que se quiere usar. Se sugiere revisar `btnUSBToggle()` para tener una referencia sobre la función de interrupción.
  - Configurar el WatchDog Timer y habilitar sus interrupciones llamando a `wdt_config()`.
  - En loop principal, llamar a `usb_loop()`, que realiza todo el funcionamiento correspondiente. Si el modo USB no se encuentra activo, `usb_loop()` sólo asigna un valor a una variable.